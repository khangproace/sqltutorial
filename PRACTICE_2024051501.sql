﻿CREATE DATABASE PRACTICE_2024051501
GO

USE PRACTICE_2024051501
GO

/**
1. Hiển thị danh sách gồm MaSV, HoTên, MaLop, DiemHP, MaHP của 
những sinh viên có điểm HP >= 5.
2. Hiển thị danh sách MaSV, HoTen , MaLop, MaHP, DiemHP được sắp 
xếp theo ưu tiên Mã lớp, Họ tên tăng dần.
3. Hiển thị danh sách gồm MaSV, HoTen, MaLop, DiemHP, MaHP của 
những sinh viên có điểm HP từ 5 đến 7 ở học kỳ I.
4. Hiển thị danh sách sinh viên gồm MaSV, HoTen, MaLop, TenLop, 
MaKhoa của Khoa có mã CNTT.
**/

CREATE TABLE tbl_STUDENT (
	studentID INT NOT NULL IDENTITY,
	PRIMARY KEY (studentID),
	studentName NVARCHAR (50) NOT NULL,
	classID NVARCHAR(10) NOT NULL,
	FOREIGN KEY (classID) REFERENCES tbl_CLASS(classID),
	majorID INT NOT NULL,
	FOREIGN KEY (majorID) REFERENCES tbl_MAJOR(majorID),
)
GO

CREATE TABLE tbl_CLASS (
	classID NVARCHAR(10) NOT NULL,
	PRIMARY KEY (classID),
)
GO

CREATE TABLE tbl_SUBJECT (
	subjectID INT NOT NULL IDENTITY,
	PRIMARY KEY (subjectID),
	subjectName NVARCHAR (30),
)
GO

CREATE TABLE tbl_SCORE (
	subjectID INT NOT NULL,
	FOREIGN KEY (subjectID) REFERENCES tbl_SUBJECT(subjectID),
	studentID INT NOT NULL,
	FOREIGN KEY (studentID) REFERENCES tbl_STUDENT(studentID),
	term INT NOT NULL,
	PRIMARY KEY (subjectID, studentID, term),
	subjectScore INT NOT NULL,
)
GO

CREATE TABLE tbl_MAJOR (
	majorID INT NOT NULL IDENTITY,
	PRIMARY KEY (majorID),
	majorName NVARCHAR(100),
)
GO
	
--INSERT CLASS
	INSERT tbl_CLASS VALUES ('10.1')
	INSERT tbl_CLASS VALUES ('10.2')
	INSERT tbl_CLASS VALUES ('10.3')
	INSERT tbl_CLASS VALUES ('10.4')
	INSERT tbl_CLASS VALUES ('10.5')
--INSERT SUBJECT
	INSERT tbl_SUBJECT VALUES ('MATH')
	INSERT tbl_SUBJECT VALUES ('STATISTIC')
	INSERT tbl_SUBJECT VALUES ('ENGLISH')
--INSERT MAJOR
	INSERT tbl_MAJOR VALUES ('CNTT')
	INSERT tbl_MAJOR VALUES ('KTXD')
	INSERT tbl_MAJOR VALUES ('KTCK')
--INSERT STUDENT

INSERT tbl_STUDENT VALUES ('A', '10.1', '1')
INSERT tbl_STUDENT VALUES ('B', '10.1', '1')
INSERT tbl_STUDENT VALUES ('C', '10.1', '1')
INSERT tbl_STUDENT VALUES ('D', '10.1', '1')
INSERT tbl_STUDENT VALUES ('E', '10.1', '1')

INSERT tbl_STUDENT VALUES ('F', '10.2', '2')
INSERT tbl_STUDENT VALUES ('G', '10.2', '2')
INSERT tbl_STUDENT VALUES ('H', '10.2', '2')
INSERT tbl_STUDENT VALUES ('I', '10.2', '2')
INSERT tbl_STUDENT VALUES ('J', '10.2', '2')

INSERT tbl_STUDENT VALUES ('K', '10.3', '3')
INSERT tbl_STUDENT VALUES ('L', '10.3', '3')
INSERT tbl_STUDENT VALUES ('M', '10.3', '3')
INSERT tbl_STUDENT VALUES ('N', '10.3', '3')
INSERT tbl_STUDENT VALUES ('O', '10.3', '3')

INSERT tbl_STUDENT VALUES ('P', '10.4', '1')
INSERT tbl_STUDENT VALUES ('Q', '10.4', '1')
INSERT tbl_STUDENT VALUES ('R', '10.4', '1')
INSERT tbl_STUDENT VALUES ('S', '10.4', '1')
INSERT tbl_STUDENT VALUES ('T', '10.4', '1')

INSERT tbl_STUDENT VALUES ('U', '10.5', '2')
INSERT tbl_STUDENT VALUES ('V', '10.5', '2')
INSERT tbl_STUDENT VALUES ('W', '10.5', '2')
INSERT tbl_STUDENT VALUES ('X', '10.5', '2')
INSERT tbl_STUDENT VALUES ('Y', '10.5', '2')
--INSERT SCORE
INSERT tbl_SCORE VALUES ('1', '2', 1, 9)
INSERT tbl_SCORE VALUES ('2', '3', 1, 8)
INSERT tbl_SCORE VALUES ('3', '4', 1, 7)
INSERT tbl_SCORE VALUES ('1', '5', 1, 6)
INSERT tbl_SCORE VALUES ('2', '6', 1, 7)
INSERT tbl_SCORE VALUES ('3', '7', 1, 8)

INSERT tbl_SCORE VALUES ('1', '8', 1, 9)
INSERT tbl_SCORE VALUES ('2', '9', 1, 8)
INSERT tbl_SCORE VALUES ('3', '10', 1, 7)
INSERT tbl_SCORE VALUES ('1', '11', 1, 6)
INSERT tbl_SCORE VALUES ('2', '12', 1, 9)

INSERT tbl_SCORE VALUES ('1', '8', 2, 9)
INSERT tbl_SCORE VALUES ('2', '9', 2, 8)
INSERT tbl_SCORE VALUES ('3', '10', 2, 7)
INSERT tbl_SCORE VALUES ('1', '11', 2, 6)
INSERT tbl_SCORE VALUES ('2', '12', 2, 9)

SELECT * FROM tbl_MAJOR
SELECT * FROM tbl_CLASS
SELECT * FROM tbl_SUBJECT
SELECT * FROM tbl_STUDENT
SELECT * FROM tbl_SCORE

-- INNER JOIN

/**
1. Hiển thị danh sách gồm MaSV, HoTên, MaLop, DiemHP, MaHP của những sinh viên có điểm HP >= 5.
**/
SElECT tbl_TEMP.studentID, tbl_TEMP.studentName, tbl_TEMP.classID, tbl_SCORE.subjectScore, tbl_SCORE.subjectID
FROM	(	
		SELECT tbl_STUDENT.studentID, tbl_STUDENT.studentName, tbl_CLASS.classID
		FROM tbl_STUDENT
		INNER JOIN tbl_CLASS ON tbl_STUDENT.classID = tbl_CLASS.classID 
		) AS tbl_TEMP
INNER JOIN tbl_SCORE ON tbl_TEMP.studentID = tbl_SCORE.studentID
WHERE tbl_SCORE.subjectScore >= 7 


/**
2. Hiển thị danh sách MaSV, HoTen , MaLop, MaHP, DiemHP được sắp xếp theo ưu tiên Mã lớp, Họ tên tăng dần.
**/
SELECT tbl_TEMP1.studentName, tbl_TEMP1.studentID, tbl_TEMP1.classID, tbl_SCORE.subjectScore, tbl_SCORE.subjectID
FROM	(
		SELECT tbl_STUDENT.studentName, tbl_STUDENT.studentID, tbl_STUDENT.classID
		FROM tbl_STUDENT
		INNER JOIN tbl_CLASS ON tbl_STUDENT.classID = tbl_CLASS.classID
		) AS tbl_TEMP1
INNER JOIN tbl_SCORE ON tbl_TEMP1.studentID = tbl_SCORE.studentID
ORDER BY tbl_TEMP1.studentName ASC, tbl_TEMP1.classID ASC


/**
3. Hiển thị danh sách gồm MaSV, HoTen, MaLop, DiemHP, MaHP của những sinh viên có điểm HP từ 5 đến 7 ở học kỳ I.
**/
SELECT tbl_TEMP.studentID, tbl_TEMP.studentNAME, tbl_TEMP.classID, tbl_SCORE.subjectScore, tbl_SCORE.subjectID, tbl_SCORE.term
FROM	(
		SELECT tbl_STUDENT.studentName, tbl_STUDENT.studentID, tbl_CLASS.classID
		FROM tbl_STUDENT
		INNER JOIN tbl_CLASS
		ON tbl_STUDENT.classID = tbl_CLASS.classID
		) AS tbl_TEMP
INNER JOIN tbl_SCORE
ON tbl_TEMP.studentID = tbl_SCORE.studentID
WHERE (tbl_SCORE.subjectScore >= 8 AND tbl_SCORE.subjectScore <= 9) AND (tbl_SCORE.term = 1)

/** 
4. Hiển thị danh sách sinh viên gồm MaSV, HoTen, MaLop,
MaKhoa của Khoa có mã CNTT.
**/
SELECT tbl_STUDENT.studentID, tbl_STUDENT.studentName, tbl_STUDENT.classID, tbl_MAJOR.majorID, tbl_MAJOR.majorName
FROM tbl_STUDENT
INNER JOIN tbl_MAJOR
ON tbl_STUDENT.majorID = tbl_MAJOR.majorID
WHERE tbl_MAJOR.majorName = 'CNTT'