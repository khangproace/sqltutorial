USE student
USE tbl_TEACHER
GO

-- UPDATE statement
UPDATE tbl_STUDENT
SET teacherID = 1
WHERE studentID = 1
GO

UPDATE tbl_STUDENT
SET teacherID = 1
WHERE studentID = 2
GO

UPDATE tbl_STUDENT
SET teacherID = 1
WHERE studentID = 3
GO

UPDATE tbl_STUDENT
SET Score = 8
WHERE studentID = 1

UPDATE tbl_STUDENT
SET Score = 6
WHERE studentID = 2

UPDATE tbl_STUDENT
SET Score = 9
WHERE studentID = 3
USE student

UPDATE tbl_STUDENT
SET Score = 2
WHERE studentID = 4

UPDATE tbl_STUDENT
SET Score = 3
WHERE studentID = 5

UPDATE tbl_STUDENT
SET Score = 4
WHERE studentID = 6

UPDATE tbl_STUDENT
SET Score = 6
WHERE studentID = 7

-- Select statement
SELECT DISTINCT studentNAME
FROM tbl_STUDENT
SELECT * FROM tbl_STUDENT
GO

-- SELECT TOP
SELECT TOP 3 studentName FROM tbl_STUDENT
SELECT TOP 100 PERCENT studentNAME from tbl_STUDENT AS tempTABLE
GO

--SELECT with condition / WHERE STATEMENT
SELECT * 
FROM tbl_STUDENT 
WHERE teacherID IS NULL


SELECT DISTINCT studentName 
FROM tbl_STUDENT AS khoaTable 
WHERE studentNAME = 'Anh Khoa'

SELECT * 
FROM tbl_STUDENT 
AS specTable 
WHERE studentNAME LIKE '%Van%' AND teacherID IS NOT NULL

SELECT COUNT(studentID) AS sumOfStudent
FROM tbl_STUDENT
AS numberOfStudent

SELECT COUNT(teacherID) AS sumOfTeacher
FROM tbl_TEACHER
AS numberOfTeacher

SELECT AVG(Score)
AS diemTB
FROM tbl_STUDENT

SELECT SUM(Score) AS diemTong
FROM tbl_STUDENT