﻿CREATE DATABASE PRACTICE2024030602
GO

USE PRACTICE2024030602
GO

/**
1. Hiển thị danh sách gồm: MaSV, HoTen, MaLop, NgaySinh 
(dd/mm/yyyy), GioiTinh (Nam, Nữ) , Namsinh của những sinh viên có họ
không bắt đầu bằng chữ N,L,T.
2. Hiển thị danh sách gồm: MaSV, HoTen, MaLop, NgaySinh 
(dd/mm/yyyy), GioiTinh (Nam, Nữ) , Namsinh của những sinh viên nam 
học lớp CT11.
3. Hiển thị danh sách gồm: MaSV, HoTen, MaLop, NgaySinh 
(dd/mm/yyyy), GioiTinh (Nam, Nữ) của những sinh viên học lớp 
CT11,CT12,CT13.
4. Hiển thị danh sách gồm: MaSV, HoTen, MaLop, NgaySinh 
(dd/mm/yyyy), GioiTinh (Nam, Nữ), Tuổi của những sinh viên có tuổi từ
19-21.
**/

CREATE TABLE tbl_STUDENT (
	studentID INT IDENTITY NOT NULL,
	PRIMARY KEY (studentID),
	studentName NVARCHAR(200) NOT NULL,
	classID  NVARCHAR(10) NOT NULL,
	DOB DATE NOT NULL,
	Gender NVARCHAR (10) NOT NULL,
)

CREATE TABLE tbl_CLASS (
	classID NVARCHAR (10) NOT NULL,
	PRIMARY KEY (classID),
)

--INSERT for tbl_STUDENT
INSERT tbl_STUDENT VALUES ('N','CT11','2004-08-22','Male')
INSERT tbl_STUDENT VALUES ('A','CT12','2003-03-31', 'Male')
INSERT tbl_STUDENT VALUES ('B','CT13','2002-04-04', 'Male')
INSERT tbl_STUDENT VALUES ('L','CT11','2004-06-19', 'Male')
INSERT tbl_STUDENT VALUES ('L','CT12','2003-08-25', 'Female')
INSERT tbl_STUDENT VALUES ('T','CT13','2002-01-30', 'Female')
INSERT tbl_STUDENT VALUES ('U','CT11','2004-09-18', 'Female')
INSERT tbl_STUDENT VALUES ('O','CT12','2003-07-13', 'Female')
INSERT tbl_STUDENT VALUES ('E','CT14','2001-05-13', 'Female')
INSERT tbl_STUDENT VALUES ('I','CT15','2002-06-29', 'Male')

--INSERT for tbl_CLASS 
INSERT tbl_CLASS VALUES ('CT11')
INSERT tbl_CLASS VALUES ('CT12')
INSERT tbl_CLASS VALUES ('CT13')
INSERT tbl_CLASS VALUES ('CT14')
INSERT tbl_CLASS VALUES ('CT15')

--ALTER
ALTER TABLE tbl_STUDENT 
	ADD FOREIGN KEY(classID) REFERENCES tbl_CLASS(classID)

DROP TABLE tbl_STUDENT

--Cau 1 
SELECT studentID, studentName, classID, Gender, DAY(DOB) AS 'DayOfBirth', YEAR(DOB) AS "YearOfBirth"
FROM tbl_STUDENT 
WHERE studentName NOT LIKE 'N' AND studentName NOT LIkE 'L' AND studentName NOT LIKE 'T'

--Cau 2
SELECT studentID, studentName, classID, Gender, DAY(DOB) AS 'DayOfBirth', YEAR(DOB) AS "YearOfBirth"
FROM tbl_STUDENT
WHERE classID LIKE 'CT11' AND GENDER LIKE 'MALE'

--Cau 3
SELECT studentID, studentName, classID, Gender, DAY(DOB) AS 'DayOfBirth'
FROM tbl_STUDENT
WHERE classID LIKE 'CT11' OR classID LIKE 'CT12' OR classID LIKE 'CT13' 

--Cau 4
SELECT studentID, studentName, classID, Gender, DAY(DOB) AS 'DayOfBirth', YEAR(GETDATE()) - YEAR(DOB) AS Age
From tbl_STUDENT
WHERE YEAR(GETDATE())-YEAR(DOB) BETWEEN 19  AND 21

