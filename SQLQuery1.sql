-- How to create DataBase, use func <CREATE DATABASE> <DATABASE NAME> Using hypercase
CREATE DATABASE student
USE student
-- How to delete a DataBase, use func <DROP DATABASE> <DATABASE NAME> Using hypercase
DROP DATABASE khang

-- How to create new TABLE, use func <CREATE TABLE> [tbl]_<NAME> 
CREATE TABLE tbl_STUDENT (
	studentID INT IDENTITY NOT NULL ,
	PRIMARY KEY (studentID),
	studentName NVARCHAR(100) NOT NULL,
)

CREATE TABLE tbl_TEACHER (
	teacherID INT IDENTITY NOT NULL,
	teacherClass NCHAR(10),
	teacherName NVARCHAR(100),
)

--How to ALTER TABLE, use func ALTER TABLE <tbl NAME> ADD <COL NAME> <Data type>

-- ALTER for tbl_STUDENT
ALTER TABLE tbl_STUDENT
			DROP CONSTRAINT PK_tbl_STUDENT
ALTER TABLE tbl_STUDENT
			DROP COLUMN studentID
ALTER TABLE tbl_STUDENT 
			ALTER COLUMN studentID INT NOT NULL
ALTER TABLE tbl_STUDENT 
			ADD studentID INT IDENTITY NOT NULL
ALTER TABLE tbl_STUDENT  
			ADD PRIMARY KEY (studentID)
ALTER TABLE tbl_STUDENT
			ADD student INT 
ALTER TABLE tbl_STUDENT
			ADD teacherID INT
ALTER TABLE tbl_STUDENT
			ADD FOREIGN KEY (teacherID) REFERENCES tbl_TEACHER (teacherID)
ALTER TABLE tbl_STUDENT
			ADD CONSTRAINT fk_teacherID
			FOREIGN KEY (teacherID) REFERENCES tbl_TEACHER (teacherID);
ALTER TABLE tbl_STUDENT
			DROP CONSTRAINT FK__tbl_STUDE__teach__6754599E
ALTER TABLE tbl_STUDENT
			ADD Score INT 

-- Command for checking constraint name
sp_help 'tbl_STUDENT'

-- ALTER for tbl_TEACHER
ALTER TABLE tbl_TEACHER
			DROP COLUMN teacherID
ALTER TABLE tbl_TEACHER
			ADD teacherID INT IDENTITY NOT NULL
ALTER TABLE tbl_TEACHER
			ADD PRIMARY KEY (teacherID)
GO

sp_help 'tbl_TEACHER'
-- How to delete a whole table
DROP TABLE tbl_STUDENT
GO

-- Select all properties from table
SELECT * FROM tbl_STUDENT
SELECT * FROM tbl_TEACHER

-- Modifying command
-- Basic command for insertion
INSERT tbl_STUDENT VALUES ('Van A')
INSERT tbl_STUDENT VALUES ('Van A')
INSERT tbl_STUDENT VALUES ('Van B')
INSERT tbl_STUDENT VALUES ('Van C')
INSERT tbl_STUDENT VALUES ('Van C')
INSERT tbl_STUDENT VALUES ('Van D')
INSERT tbl_STUDENT VALUES ('Van E')
INSERT tbl_STUDENT VALUES ('Van F')
INSERT tbl_STUDENT VALUES ('Van F')

INSERT tbl_TEACHER VALUES ('10A1', 'Le Thi A')
INSERT tbl_TEACHER VALUES ('10A2', 'Le Thi B')
INSERT tbl_TEACHER VALUES ('10A3', 'Le Thi C')

-- Basic command for delete with specific condition
DELETE tbl_STUDENT WHERE studentID = 1
DELETE tbl_STUDENT

SELECT * FROM tbl_TEACHER
SELECT * FROM tbl_STUDENT
